﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using CountND.Models;

namespace CountND.Data
{
    public partial class dataContext : DbContext
    {
        public dataContext()
        {
        }

        public dataContext(DbContextOptions<dataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Vendorlist> Vendorlists { get; set; } = null!;
        public virtual DbSet<列出首重續重金額> 列出首重續重金額s { get; set; } = null!;
        public virtual DbSet<貨件> 貨件s { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlite("DataSource=data.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Vendorlist>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("Vendorlist");

                entity.Property(e => e.AccountNumber).HasColumnName("accountNumber");

                entity.Property(e => e.AdditionalWeightKg).HasColumnName("additionalWeightKg");

                entity.Property(e => e.CodeName).HasColumnName("codeName");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.InitialWeightKg).HasColumnName("initialWeightKg");

                entity.Property(e => e.Password).HasColumnName("password");

                entity.Property(e => e.VendorId).HasColumnName("vendorId");

                entity.Property(e => e.VendorType).HasColumnName("vendorType");
            });

            modelBuilder.Entity<列出首重續重金額>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("列出首重續重金額");

                entity.Property(e => e.AdditionalWeightKg).HasColumnName("additionalWeightKg");

                entity.Property(e => e.InitialWeightKg).HasColumnName("initialWeightKg");
            });

            modelBuilder.Entity<貨件>(entity =>
            {
                entity.HasKey(e => e.識別碼);

                entity.ToTable("貨件");

                entity.Property(e => e.識別碼).ValueGeneratedNever();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
