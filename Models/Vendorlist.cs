﻿using System;
using System.Collections.Generic;

namespace CountND.Models
{
    public partial class Vendorlist
    {
        public string? VendorId { get; set; }
        public string? CodeName { get; set; }
        public string? Description { get; set; }
        public string? VendorType { get; set; }
        public long? InitialWeightKg { get; set; }
        public long? AdditionalWeightKg { get; set; }
        public string? AccountNumber { get; set; }
        public string? Password { get; set; }
    }
}
