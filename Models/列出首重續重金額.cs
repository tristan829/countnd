﻿using System;
using System.Collections.Generic;

namespace CountND.Models
{
    public partial class 列出首重續重金額
    {
        public string? 建立時間 { get; set; }
        public string? 提單號碼 { get; set; }
        public string? 序號 { get; set; }
        public string? 貨件狀態 { get; set; }
        public string? 狀態時間 { get; set; }
        public string? 司機 { get; set; }
        public string? 廠商名稱 { get; set; }
        public string? 客戶名稱 { get; set; }
        public string? 重量 { get; set; }
        public string? 代收 { get; set; }
        public string? 稅金 { get; set; }
        public string? 轉站 { get; set; }
        public string? 運費總金額 { get; set; }
        public long? InitialWeightKg { get; set; }
        public long? AdditionalWeightKg { get; set; }
    }
}
